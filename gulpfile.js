'use strict'

const gulp = require('gulp')
const browserSync = require('browser-sync')
const sass = require('gulp-sass')
const sourcemaps = require('gulp-sourcemaps')
const prefix = require('gulp-autoprefixer')
const cleanCSS = require('gulp-clean-css')
const plumber = require('gulp-plumber')
const imagemin = require('gulp-imagemin')
const cache = require('gulp-cache')

gulp.task('sass', () => {
  return gulp.src('app/assets/sass/main.sass')
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(sass())
    .pipe(prefix({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(cleanCSS())
    .pipe(plumber.stop())
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('app/assets/css'))
    .pipe(browserSync.stream())
})

gulp.task('img', () => {
  return gulp.src('app/assets/img/**/*')
    .pipe(cache(imagemin([
      imagemin.gifsicle({interlaced: true}),
      imagemin.jpegtran({progressive: true}),
      imagemin.optipng({optimizationLevel: 5}),
      imagemin.svgo({plugins: [{removeViewBox: true}]})
    ])))
    .pipe(gulp.dest('app/assets/img'))
    .pipe(browserSync.stream())
})

gulp.task('serve', ['sass', 'img'], () => {
  browserSync.init({
    server: './app/',
    ghostMode: true
  })

  gulp.watch('app/assets/sass/**/*.sass', ['sass'])
  gulp.watch('app/assets/js/**/*.js', ['js'])
  gulp.watch('app/assets/img/*', ['img'])
  gulp.watch('app/*.html').on('change', browserSync.reload)
});

// Tarefa para usar, antes de subir em produção
gulp.task('cssmin', () => {
  return gulp.src('app/assets/css/**/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}, (details) => {
      console.log(details.name + ': ' + details.stats.originalSize)
      console.log(details.name + ': ' + details.stats.minifiedSize)
    }))
    .pipe(gulp.dest('app/assets/css'))
})

gulp.task('default', ['serve'])
