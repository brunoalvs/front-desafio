# Tela de Login - Desafio Petrobras

## Como usar

É necessário que o Node (versão 6+) esteja instalado no ambiente. Caso não tenha instalado acesse o site do [NodeJs](https://nodejs.org/) e instale.
Instale globalmente o Gulp com o comando `npm install -g gulp-cli`;

  1.
    Instale todas as dependencias de desenvolvimento utilizando o comando `npm install`;

  2.
    Ainda no terminal use o comando `gulp` para gerar os arquivos compilados e subir um servidor com o browser Sync.

  3.
    Os arquivos compilados estarão na pasta 'APP'. Antes de subir em produção, use o comando `gulp cssmin` para remover os comentários e o sourcemaps.
